#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_syscall_button_clicked();

    void on_rdi_button_toggled(bool checked);

    void on_rsi_button_toggled(bool checked);

    void on_rdx_button_toggled(bool checked);

    void on_r10_button_toggled(bool checked);

    void on_r8_button_toggled(bool checked);

    void on_r9_button_toggled(bool checked);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
