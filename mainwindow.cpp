#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QCheckBox>
#include <QLineEdit>
#include <QSqlTableModel>

#include <string>

#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <signal.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("syscall");
}

MainWindow::~MainWindow()
{
    delete ui;
}

#define GET_REGISTER(cb,le, ret){                               \
    if(!(cb)->isChecked())   (ret)=(le)->text().toLong();       \
    else (ret)=long((le)->text().toLocal8Bit().data());         \
    }



void MainWindow::on_syscall_button_clicked()
{
    //x86-64        rdi   rsi   rdx   r10   r8    r9    -

    long syscall_n=ui->rax_edit->text().toLong();

    long rdi, rsi, rdx, r10, r8, r9;

    GET_REGISTER(ui->rdi_button,ui->rdi_edit, rdi);
    GET_REGISTER(ui->rsi_button,ui->rsi_edit, rsi);
    GET_REGISTER(ui->rdx_button,ui->rdx_edit, rdx);
    GET_REGISTER(ui->r10_button,ui->r10_edit, r10);
    GET_REGISTER(ui->r8_button,ui->r8_edit, r8);
    GET_REGISTER(ui->r9_button,ui->r9_edit, r9);


    long ret=syscall(syscall_n, rdi, rsi, rdx, r10, r8, r9);

    ui->rax_edit->setText(QString::number(ret));

    ui->errno_edit->setText(strerror(errno));
}

#define update_text(){                                                              \
                        QCheckBox* button = qobject_cast<QCheckBox*>(sender());     \
                        if(checked)                                                 \
                            button->setText("buffer");                              \
                        else                                                        \
                            button->setText("int");                                 \
                        }

void MainWindow::on_rdi_button_toggled(bool checked)
{
    update_text();
}

void MainWindow::on_rsi_button_toggled(bool checked)
{
    update_text();
}

void MainWindow::on_rdx_button_toggled(bool checked)
{
    update_text();
}

void MainWindow::on_r10_button_toggled(bool checked)
{
    update_text();
}

void MainWindow::on_r8_button_toggled(bool checked)
{
    update_text();
}

void MainWindow::on_r9_button_toggled(bool checked)
{
    update_text();
}


